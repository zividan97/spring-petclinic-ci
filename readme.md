# **Continuous Integration Solution - Idan Ziv**
## Implement CI process including tests
I chose Travis CI as the service to implement.<br />
Git was already installd on my machine, and I used it through VS Code IDE.

### Part 1 - Git Clone
a. cloned the GitHub repository into my GitLab account using the web interface of both websites.<br />
b. deleted the original .travis.yml file.<br />
c. cloned the GitLab repository into my local machine using GitLab's automatic Visual Studio clone script.

### Part 2 - Travis CI & GitLab Accounts Integration
a. I registered to Travis CI using my GitLab account.<br />
b. In Travis CI's website I activated the GitLab repository.

### Part 3 - Creating The .travis.yml file
a. I created a `.travis.yml` file in my IDE<br />
b. As described in the assignment, the app uses java as it's language. So I added this line to the file:
```yaml
language: java
```
c. From the readme file I understood that the app uses java 8 full JDK so I added:
```yaml
jdk: oraclejdk8
```
d. The repository has a `docker-compose.yml` file. Travis CI supports it automaticly, so all I had to add is:
```yaml
services:
  - docker
```
e. Now I've tried to commit and push, but the build errored in Travis CI system.<br />
f. I decided to take a peek at the original travis file (I know it's kind of cheating). I looked at the commit history of the file and saw a commit named `Attempt to fix travis build by hardcoding Ubuntu dist`. inside I've seen they only added this line, and so did I:
```yaml
dist: trusty
```
Of course I searched for it in the documentation and figured that it's a build enviroment that has the right JDK installed, and that's why it was needed. I also figured that the other lines in the original file are related to pushing the project to Docker Hub, which is not required for me.<br />
g. Commit and push, now the build passed.
